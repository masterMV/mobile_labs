//
//  SecondView.swift
//  Lab
//
//  Created by maxim vingalov on 06/11/2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class SecondView: UIView {

    weak var controller: SecondViewController!
    
    fileprivate lazy var label: UILabel = {
        
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        
        return label
        
    }()
    
    fileprivate lazy var changeColorButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Change bg color", for: .normal)
        button.addTarget(controller, action: #selector(SecondViewController.changePrevScreenBG), for: .touchUpInside)
        
        return button
        
    }()
    
    init(with controller: SecondViewController) {
        super.init(frame: .zero)
        
        self.controller = controller
        
        self.backgroundColor = .white
        
        addSubview(label)
        addSubview(changeColorButton)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutUI()
        
    }
    
    func layoutUI() {
        
        label.frame = CGRect(x: 20, y: 100, width: bounds.width - 40, height: 40)
        changeColorButton.frame = CGRect(x: 20, y: 200, width: bounds.width - 40, height: 60)
        
    }
    
    func setText(text: String) {
        
        self.label.text = text
        
    }
    
}
