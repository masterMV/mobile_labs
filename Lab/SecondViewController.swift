//
//  SecondViewController.swift
//  Lab
//
//  Created by maxim vingalov on 06/11/2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    weak var firstVC: FirstViewController?
    
    fileprivate var passedText: String!
    
    fileprivate var mainView: SecondView {
        return view as! SecondView
    }
    
    override func loadView() {
        self.view = SecondView(with: self)
    }
    
    init(text: String) {
        super.init(nibName: nil, bundle: nil)
        self.passedText = text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainView.setText(text: passedText)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func changePrevScreenBG() {
        
        firstVC?.changeBGColor(color: .purple)
        dismiss(animated: true, completion: nil)
        
    }
    
    deinit {
        print("deinit SecondViewController")
    }
    
}
