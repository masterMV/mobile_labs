//
//  FirstView.swift
//  Lab
//
//  Created by maxim vingalov on 06/11/2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class FirstView: UIView {

    weak var controller: FirstViewController!
    
    fileprivate lazy var redButton: UIButton = {
       
        let button = UIButton(type: .system)
        button.setTitle("Red", for: .normal)
        button.tag = 0
        button.addTarget(self, action: #selector(buttonDidTap(sender:)), for: .touchUpInside)
        
        return button
        
    }()
    
    fileprivate lazy var yellowButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Yellow", for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(buttonDidTap(sender:)), for: .touchUpInside)

        return button
        
    }()

    fileprivate lazy var greenButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Green", for: .normal)
        button.tag = 2
        button.addTarget(self, action: #selector(buttonDidTap(sender:)), for: .touchUpInside)

        return button
        
    }()
    
    fileprivate lazy var textField: UITextField = {
        
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "text for passing to next screen"
        textField.textAlignment = .center
        textField.delegate = self
        
        return textField
        
    }()
    
    fileprivate lazy var passButton: UIButton = {
        
        let button = UIButton(type: .system)
        button.setTitle("Pass data", for: .normal)
        button.addTarget(controller, action: #selector(FirstViewController.passButtonDidTap), for: .touchUpInside)
        
        return button
        
    }()

    init(with controller: FirstViewController) {
        super.init(frame: .zero)
        
        self.controller = controller
        
        self.backgroundColor = .white
        
        addSubview(redButton)
        addSubview(yellowButton)
        addSubview(greenButton)
        addSubview(textField)
        addSubview(passButton)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutUI()
        
    }
    
    func layoutUI() {
        
        redButton.frame = CGRect(x: 20, y: 100, width: bounds.width - 40, height: 60)
        yellowButton.frame = CGRect(x: 20, y: 200, width: bounds.width - 40, height: 60)
        greenButton.frame = CGRect(x: 20, y: 300, width: bounds.width - 40, height: 60)
        textField.frame = CGRect(x: 20, y: 400, width: bounds.width - 40, height: 40)
        passButton.frame = CGRect(x: 20, y: 500, width: bounds.width - 40, height: 60)

    }
    
    func getText() -> String {
        return textField.text!
    }
    
    //MARK: - UI Handlers
    
    @objc func buttonDidTap(sender: UIButton) {
        
        switch sender.tag {
        case 0:
            self.backgroundColor = .red
        case 1:
            self.backgroundColor = .yellow
        case 2:
            self.backgroundColor = .green
        default:
            break
        }
        
    }
    
    
}

extension FirstView: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
