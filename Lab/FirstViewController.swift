//
//  ViewController.swift
//  Lab
//
//  Created by maxim vingalov on 06/11/2018.
//  Copyright © 2018 maxim vingalov. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    fileprivate var mainView: FirstView {
        return view as! FirstView
    }
    
    override func loadView() {
        self.view = FirstView(with: self)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()



    }
    
    func changeBGColor(color: UIColor) {
        
        self.mainView.backgroundColor = color
        
    }

    
    //MARK: - UI Handlers
    
    @objc func passButtonDidTap() {

        let nextVC = SecondViewController(text: mainView.getText())
        nextVC.firstVC = self
        
        self.present(nextVC, animated: true, completion: nil)
        
    }

}

